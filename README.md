# React and Morty

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Heroku](http://heroku-badge.herokuapp.com/?app=heroku-badge&root=projects.html)](http://reackt-and-morty.herokuapp.com/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

An application that fetches and displays character info of Rick and Morty characters from [The Rick and Morty API](https://rickandmortyapi.com/).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Learn More](#learn-more)
- [Components](#components)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [Licence](#licence)

## Install

This project uses [node](http://nodejs.org) and [npm](https://npmjs.com).

```
npm install
```

## Usage

### Development server
```
npm run dev
```
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Build

```
npm run build
```
Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Serve build
```
npm run start
```

### Test
```
npm run test
```
Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Eject
```
npm run eject
```
**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Components

There are two routes in our app, displaying the dashboard and character details. The **Dashboard** component will use the **CardList** component, while the **Profile** component fetches and displays character details.

## Maintainers

[Marius S. Lode @Marius-L](https://gitlab.com/Marius-L) </br>
[Sindre S. Kjær @sindrekjr](https://gitlab.com/sindrekjr)

## Contributing

PRs accepted. Please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) and [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specifications.

## Licence

The MIT License (MIT)

MIT (c) Marius S. Lode, Sindre S. Kjær
