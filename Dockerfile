FROM node:lts-alpine

ARG api_path

ENV PORT 3000
ENV REACT_APP_RM_API=$api_path

WORKDIR /src
COPY package.json .
RUN npm install
COPY . .

RUN npm run build

EXPOSE 3000/tcp

CMD [ "npm", "start" ]
