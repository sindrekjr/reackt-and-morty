import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Dashboard from './Dashboard';
import Profile from './Profile';

import './App.css';
import { createMuiTheme, CssBaseline } from '@material-ui/core';
import { ThemeProvider } from "@material-ui/styles";

const customDarkTheme = createMuiTheme({
  palette: {
    type: "dark"
  },
  typography: {
    fontWeightRegular: 700,
    fontWeightLight: 700,
    fontFamily: "Roboto",
    h4: {
      marginTop: 20 
    }
  }
});

const App = () => {
  return (
    <ThemeProvider theme={customDarkTheme}>
      <CssBaseline />
      <Router>
        <div className="App">
          <Link to="/">
            <header className="App-header"></header>
            <div className="Header-border"></div>
          </Link>
        </div>
        <main>
          <Switch>
            <Route path="/profile/:id">
              <Profile />
            </Route>
            <Route path="/">
              <Dashboard />
            </Route>
          </Switch>
        </main>
      </Router>
    </ThemeProvider>
  );
}

export default App;
