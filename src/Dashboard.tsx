import React, { Component } from 'react';
import Debounce from 'react-debounce-component';

import { Input, Container } from '@material-ui/core';
import CardList from './CardList';

export default class Dashboard extends Component {
  state = {
    searchFieldValue: ''
  };

  search(e: any) {
    this.setState({ searchFieldValue: e.currentTarget.value });
  }

  render() {
    return (
      <Container>
        <div className="search">
          <Input fullWidth={true} type="search" placeholder="Search for a character..." value={this.state.searchFieldValue} onChange={event => this.search(event)}></Input>
        </div>
        <Debounce ms={500}>
          <CardList filter={this.state.searchFieldValue} />
        </Debounce>
      </Container>
    );
  }
}
