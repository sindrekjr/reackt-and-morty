import React, { Component } from 'react';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

import { ProfileDetails } from './models';

import { Button, Card, CardMedia, CardContent, Typography, Table, TableBody, TableRow, TableCell, Box } from "@material-ui/core";

import './CardList.css';

class CardList extends Component {
  public static propTypes = {
    filter: PropTypes.string
  };

  state = {
    characters: [],
    loadCount: 20,
    filter: ''
  };

  componentDidMount() {
    this.fetchData().then(characterEndpoint => {
      let query = '';
      for(let i = 1; i <= characterEndpoint.info.count; i++) {
        if(i > 1) query += ',';
        query += i;
      }

      this.fetchData(`${process.env.REACT_APP_RM_API}/character/${query}`).then(allCharacters => {
        this.setState({ 
          characters: allCharacters,
          loadCount: characterEndpoint.results.length
        });
      });
    });
  }

  fetchData(url: string = `${process.env.REACT_APP_RM_API}/character`): Promise<any> {
    return fetch(url).then(response => response.json());
  }

  loadMore(amount: number = 20) {
    this.setState({ loadCount: this.state.loadCount + amount });
  }

  render() {
    if(this.state.characters.length) {
      return (
        <div className="wrap">
          <Box flexWrap="wrap" display="flex" justifyContent="center">
            {this.renderCards(this.state.characters)}
          </Box>
          <Button onClick={() => this.loadMore()}>Load more...</Button>
        </div>
      );
    } else {
      return <h1>Loading...</h1>
    }
  }

  renderCards(characters: ProfileDetails[]) {
    const { filter }: any = this.props;
    const { loadCount }: any = this.state;

    const elements: ProfileDetails[] = 
      [
        // Spread characters array to make sure resulting array is flat
        ...characters
        // Filter the known characters by given search string
        .filter(c => c.name.toLowerCase().includes(filter.toLowerCase()))
      ]
      // Slice cuts off the filtered array by the amount we want to load
      .slice(0, loadCount);

    const output: any[] = [];
    for(let e of elements) {
      output.push(<CharacterCard key={`cha${e.id}`} character={e} />);
    }

    return output;
  }
}

class CharacterCard extends Component {
  public static propTypes = {
    character: PropTypes.object
  };

  render() {
    const { character }: any = this.props;
    const cardStyle = {
      root: {
        width: 250,
        margin: 15,
        alignItems: "flex-start"
      },
      media: {
        height: 250
      }
    };

    return (
      <Card style={cardStyle.root}>
        <Link to={`/profile/${character.id}`}>
          <CardMedia
            style={cardStyle.media}
            image={character.image}
            title={character.name}
          />
        </Link>
        <CardContent>
          <Typography variant="h5">
            {character.name}
          </Typography>
          <Table className="test">
            <TableBody>
              <TableRow>
                <TableCell>Status: </TableCell>
                <TableCell align="right">{character.status}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Species: </TableCell>
                <TableCell align="right">{character.species}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Gender: </TableCell>
                <TableCell align="right">{character.gender}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Origin: </TableCell>
                <TableCell align="right">{character.origin.name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Last location: </TableCell>
                <TableCell align="right">{character.location.name}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </CardContent>
      </Card>
    );
  }
}

export default CardList;
