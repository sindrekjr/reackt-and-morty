export interface ProfileDetails {
  id: number;
  name: string;
  status: string;
  image: string;
  gender: string;
  species: string;
  origin: Origin;
  location: Location;
}

export interface Origin {
  name: string;
}

export interface Location {
  name: string;
  url: string;
  type: string;
  dimension: string;
}
