import React from 'react';
import { Container, Table, TableBody, TableRow, TableCell, Typography, Box } from '@material-ui/core';
import { ProfileDetails } from "./models";
import { withRouter, RouteComponentProps } from 'react-router-dom';

const imageStyle = {
  marginRight: 20,
  borderRadius: "50%"
}

type PathParamsType = {
  id: string,
}

type PropsType = RouteComponentProps<PathParamsType>

class Profile extends React.Component<PropsType> {
  profile?: ProfileDetails;

  state = {
    loading: true
  }
  
  componentDidMount() {
    fetch(`${process.env.REACT_APP_RM_API}/character/${this.props.match.params.id}`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      this.profile = data;
      if(this.profile) {
        this.setState({loading: true});
        fetch(this.profile.location.url)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          if(this.profile) {
            this.profile.location = data;
            this.setState({loading: false});
          }
        })
      }
    });
  }

  render() {
    if(this.state.loading || !this.profile) {
      return <h1>Loading...</h1>
    } else {
      return (
        <Container maxWidth="md">
          <Table className="test">
            <TableBody>
              <TableRow>
                <TableCell>
                  <Box display="flex" flexDirection="row">
                    <img style={imageStyle} src={this.profile.image} alt={this.profile.name} height="196" width="196" />
                    <Box display="flex" flexDirection="column" justifyContent="flex-end">
                      <Typography variant="h2">{this.profile.name}</Typography>
                      <Typography>Status: {this.profile.status}</Typography>
                    </Box>
                  </Box>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Typography variant="h4">Profile</Typography>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>Gender: </TableCell>
                <TableCell align="right">{this.profile.gender}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Species: </TableCell>
                <TableCell align="right">{this.profile.species}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Origin: </TableCell>
                <TableCell align="right">{this.profile.origin.name}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Typography variant="h4">Location</Typography>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>Current location: </TableCell>
                <TableCell align="right">{this.profile.location.name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Type: </TableCell>
                <TableCell align="right">{this.profile.location.type}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Dimension: </TableCell>
                <TableCell align="right">{this.profile.location.dimension}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Container>
      );
    }
  }
}

export default withRouter(Profile);
