/**
 * Dependencies
 * @ignore
 */
const express = require('express');
const hist = require('connect-history-api-fallback');
const path = require('path');

/**
 * App instance
 * @ignore
 */
const { PORT: port = 3000 } = process.env;
const app = express();

app.use(hist());
app.use(express.static(__dirname + '/build'));
app.listen(port, () => console.log(`Listening on port ${port}.`));
